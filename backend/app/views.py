from app.models import *


def get_parent_sections(building_id: int) -> list[Section]:
    AllSect = Section.objects.values_list("id", flat=True)  # список всех секций
    ChildSect = Section.objects.values_list("parent", flat=True).exclude(parent__isnull=True).distinct()  # список дочерних секций без повторений
    Sect = AllSect.intersection[ChildSect]  # список родительских секций
    ObjSect = Section.objects.filter(id__in=Sect)  # объекты родительских секций

    for i in ObjSect:
        pricing = Expenditure.objects.filter(section_id = i[id])
        budget = 0
        for y in pricing:
            budget = y.count * y.price #  бюджет расценок секции



